import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { tmdbConfig } from '../../config/tmdb.config';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MovieDto } from '../dto/create-movie.dto';

@Injectable()
export class MoviesRepository {
  constructor(private readonly httpService: HttpService) {}

  findPopularMovies(): Observable<MovieDto[]> {
    try {
      const popularMovies = this.httpService
      .get(`${tmdbConfig.baseURL}/movie/popular`, {
        params: {
          api_key: tmdbConfig.apiKey,
        },
      })
      .pipe(
        map((response: AxiosResponse) => response.data.results as MovieDto[]),
      );

      console.log(popularMovies);

      return popularMovies;
    } catch (error) {
      throw new Error(error.message);
    }

  }

  findMovieById(id: number): Observable<MovieDto> {
    try {
      return this.httpService
      .get(`${tmdbConfig.baseURL}/movie/${id}`, {
        params: {
          api_key: tmdbConfig.apiKey,
        },
      })
      .pipe(
        map((response: AxiosResponse) => response.data as MovieDto),
      );
    } catch (error) {
      throw new Error(error.message);
    }
  }
}
