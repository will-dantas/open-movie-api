import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MoviesRepository } from './repositories/movies.repository';
import { Movie } from './models/movie.model';

@Injectable()
export class MoviesService {
  constructor(private readonly moviesRepository: MoviesRepository) {}

  getPopularMovies(): Observable<Movie[]> {
    return this.moviesRepository.findPopularMovies().pipe(
      map((movies) =>
        movies.map(
          (movie) =>
            new Movie(
              movie.id,
              movie.title,
              movie.overview,
              movie.release_date,
              movie.poster_path,
            ),
        ),
      ),
    );
  }

  getMovieById(id: number): Observable<Movie> {
    return this.moviesRepository.findMovieById(id).pipe(
      map(
        (movie) =>
          new Movie(
            movie.id,
            movie.title,
            movie.overview,
            movie.release_date,
            movie.poster_path,
          ),
      ),
    );
  }
}
