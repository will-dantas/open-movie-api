import { Controller, Get, Param } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Movie } from './models/movie.model';
import { MoviesService } from './movies.service';

@Controller('movies')
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) {}

  @Get('popular')
  getPopularMovies(): Observable<Movie[]> {
    return this.moviesService.getPopularMovies();
  }

  @Get(':id')
  getMovieById(@Param('id') id: number): Observable<Movie> {
    return this.moviesService.getMovieById(id);
  }
}
