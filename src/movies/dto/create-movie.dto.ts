export class MovieDto {
  id: number;
  title: string;
  overview: string;
  release_date: string;
  poster_path: string;
}